//   Home Page JS

$(function () {
$('.main-banner').owlCarousel({
    loop:true,
    nav: true,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    dots: true,
    animateOut: 'fadeOut',
    autoplay:true,
    autoplayTimeout:15000,
    autoplayHoverPause:false,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
        },                   
        574:{
            items:1,
        },
        766: {
            items: 1,
        }, 
        992: {
            items: 1,
        }
    }
});
$('.card').on('click', function(){
    $('.card.current').removeClass('current');
    $(this).addClass('current');
});

$('.navbar-nav .nav-item').on('click', function(){
    $('.nav-item.active').removeClass('active');
    $(this).addClass('active');
});
// $('.navbar-light .navbar-toggler').click(function(){
//     $('#navbarSupportedContent').toggle('show');
// });

// $('.find-out-accordion .card .card-header button').click(function(){
//     $('.collapse').toggleClass('show');
// });
});


